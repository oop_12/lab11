package com.kulis;

public interface Flyable {
    public void fly();
    public void landing();
    public void takeoff();
}