package com.kulis;

public class App {
    public static void main(String[] args) {
        
        Bat bat1 = new Bat("Sashimi");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Bird bird1 = new Bird("Hashina");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();

        Cat cat1 = new Cat("Nelo");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        cat1.swim();

        Crocodile crocodile1 = new Crocodile("Miso");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.crawl();

        Dog dog1 = new Dog("Fuyu");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.swim();

        Fish fish1 = new Fish("Chiba");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Rat rat1 = new Rat("Baji");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        rat1.swim();

        Snake snake1 = new Snake("Sekiro");
        snake1.eat();
        snake1.sleep();
        snake1.swim();
        snake1.crawl();

        Plane plane1 = new Plane("Sakura", "Kuro");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Superman superman1 = new Superman("Okami");
        superman1.sleep();
        superman1.takeoff();
        superman1.fly();
        superman1.landing();
        superman1.walk();
        superman1.run();
        superman1.swim();

        Human human1 = new Human("Ashina");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();
        human1.swim();

        System.out.println();
        Flyable[] flyables = {bat1,bird1, plane1, superman1 };
        for(int i=0; i<flyables.length;i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = { bird1,cat1,dog1, rat1,superman1, human1 };
        for(int i=0; i<walkables.length;i++){
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = {superman1,snake1,rat1,cat1,dog1,fish1,crocodile1, human1 };
        for(int i=0; i<swimables.length;i++){
            swimables[i].swim();
        }

        Crawlable[] crawlables = {crocodile1, snake1 };
        for(int i=0; i<crawlables.length;i++){
            crawlables[i].crawl();
        }
    }
}